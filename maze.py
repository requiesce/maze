import cocos
from cocos.director import director
from cocos.scenes.transitions import FadeTRTransition, SplitColsTransition
from collections import defaultdict
from pyglet.window import key
import cocos.collision_model as CollisionManager
import cocos.euclid as Euclid
import cocos.actions as Actions
import random as Randomiser
import pygame as Pygame
import pyglet as Pyglet
#import time as Time
#from time import sleep
#from threading import Timer


# RANDOMISER - USED BY OVERLAYLAYER (endpoint hint), GAMELAYER (endpoint, enemy)
randomiser = Randomiser.randrange(0, 3)
endpoint_locations = ((840, 120), (90, 300), (80, 120))

# SOUNDS
Pygame.mixer.init(frequency=22050, size=-16, channels=6, buffer=4096)
Pygame.mixer.set_reserved(6)
reserveChannel = Pygame.mixer.Channel(0)
reserveChannel.set_volume(1.0)
backgroundChannel = Pygame.mixer.Channel(1)
backgroundChannel.set_volume(1.0)
collisonChannel = Pygame.mixer.Channel(2)
collisonChannel.set_volume(0.3)
priorityChannel = Pygame.mixer.Channel(3)
priorityChannel.set_volume(2.0)
priorityChannel2 = Pygame.mixer.Channel(4)
priorityChannel2.set_volume(2.0)
alarmChannel = Pygame.mixer.Channel(5)
alarmChannel.set_volume(0.7)
sound_magic = Pygame.mixer.Sound('GameSounds/SoundPickupMagic.ogg')
sound_teleport = Pygame.mixer.Sound('GameSounds/SoundTeleport.ogg')
sound_hit = Pygame.mixer.Sound('GameSounds/SoundCut.ogg')
sound_alarm = Pygame.mixer.Sound('GameSounds/SoundEmergencyAlert.ogg')
sound_break = Pygame.mixer.Sound('GameSounds/SoundBreak.ogg')
sound_scream = Pygame.mixer.Sound('GameSounds/SoundScream.ogg')
sound_cheer = Pygame.mixer.Sound('GameSounds/SoundCheer.ogg')
song1 = Pygame.mixer.Sound('GameSounds/MusicKisnouNewDescent.ogg')


# ACTOR
class Actor(cocos.sprite.Sprite):
    def __init__(self, image, x, y, ):
        super(Actor, self).__init__(image)
        sprite_size = 30
        self.position = Euclid.Vector2(x, y)
        self.cshape = CollisionManager.AARectShape(self.position,
                                     sprite_size * 0.5,
                                     sprite_size * 0.5)

    def move(self, offset):
        self.position += offset
        self.cshape.center += offset

    def update(self, elapsed):
        pass

    def collide(self, other):
        pass


# PLAYER; INHERITS FROM ACTOR
class Player(Actor):
    KEYS_PRESSED = defaultdict(int)

    def __init__(self, x, y, name):
        super(Player, self).__init__('GameImages/player-extraextrasmall-white.png', x, y)

        self.speedVertical = Euclid.Vector2(0, 250)
        self.speedHorizontal = Euclid.Vector2(250, 0)
        self.collide_Wall = False
        self.health = 80
        self.is_alive = True
        self.direction = 'up'
        self.show_view()
        self.alarmCount = 0
        self.isBroken = False
        self.name = name
        self.menuTimer = 500
        self.menuUp = True
        print (name)

    def update(self, elapsed):
        # PREVENTS MULTIPLE CALLS OF MENU
        #print(self.menuTimer)
        if self.menuTimer > 0:
            self.menuTimer -= 1
        elif self.menuTimer <= 0 and self.menuUp == True:
            self.menuTimer = 500
            self.menuUp = False
        if self.is_alive == True:
            pressed = Player.KEYS_PRESSED
            movementVertical = pressed[key.UP] - pressed[key.DOWN]
            movementHorizontal = pressed[key.RIGHT] - pressed[key.LEFT]
            if pressed[key.UP] and not self.collide_Wall:
                if self.direction != 'up':
                    if self.direction == 'right':
                        rotate = (Actions.RotateBy(-90, duration=0))
                    if self.direction == 'down':
                        rotate = (Actions.RotateBy(180, duration=0))
                    if self.direction == 'left':
                        rotate = (Actions.RotateBy(90, duration=0))
                    if self.are_actions_running() == False:
                        self.do(rotate)
                self.move(self.speedVertical * movementVertical * elapsed)
                self.direction = 'up'
            elif pressed[key.DOWN] and not self.collide_Wall:
                if self.direction != 'down':
                    if self.direction == 'right':
                        rotate = (Actions.RotateBy(90, duration=0))
                    if self.direction == 'up':
                        rotate = (Actions.RotateBy(180, duration=0))
                    if self.direction == 'left':
                        rotate = (Actions.RotateBy(-90, duration=0))
                    if self.are_actions_running() == False:
                        self.do(rotate)
                self.move(self.speedVertical * movementVertical * elapsed)
                self.direction = 'down'
            elif pressed[key.RIGHT] and not self.collide_Wall:
                if self.direction != 'right':
                    if self.direction == 'up':
                        rotate = (Actions.RotateBy(90, duration=0))
                    if self.direction == 'down':
                        rotate = (Actions.RotateBy(-90, duration=0))
                    if self.direction == 'left':
                        rotate = (Actions.RotateBy(180, duration=0))
                    if self.are_actions_running() == False:
                        self.do(rotate)
                self.move(self.speedHorizontal * movementHorizontal * elapsed)
                self.direction = 'right'
            elif pressed[key.LEFT] and not self.collide_Wall:
                if self.direction != 'left':
                    if self.direction == 'right':
                        rotate = (Actions.RotateBy(-180, duration=0))
                    if self.direction == 'up':
                        rotate = (Actions.RotateBy(-90, duration=0))
                    if self.direction == 'down':
                        rotate = (Actions.RotateBy(90, duration=0))
                    if self.are_actions_running() == False:
                        self.do(rotate)
                self.move(self.speedHorizontal * movementHorizontal * elapsed)
                self.direction = 'left'
            elif pressed[key.ENTER]:
                ####### NEED TO CHECK IF KEY IS HELD DOWN HERE #####
                # IF HELD DOWN, DO NOT CALL MENU AGAIN
                # calling pygame.init freezes; requires another solution
                #Pygame.init()
                #keys = Pygame.key.get_pressed()
                #if keys[Pygame.K_RETURN]:
                if self.menuUp != True:
                    hud_layer.generate_menu()
                    self.menuUp = True
            elif pressed[key.ESCAPE]:
                Pyglet.app.exit()
            self.collide_Wall = False
            self.cshape.center = self.position

    def checkHealth(self):
        return self.health

    def teleport(self):
        self.position = (400, 400)
        self.cshape.center = self.position

    def collide(self, other):
        # COLLISION VS ENEMIES
        health = self.health
        if isinstance(other, Enemy):
            overlay_layer.healthBar(health)
            # print('i am colliding with ' + str(other))
            if self.health > 0:
                # print("player health is: " + str(self.health))
                self.health -= 0.5
            elif self.health <= 0:
                print("you are DEAD")
                if self.isBroken == False:
                    reserveChannel.play(sound_break)
                    # STOP TIMER
                    # call stop_timer from GameLayer
                    #GameLayer.stop_timer()
                    ###################################################
                    ###################################################
                    self.isBroken = True
                priorityChannel.play(sound_scream)
                priorityChannel2.play(sound_scream)
                hud_layer.show_screen_message('DEAD')
                if self.direction == 'up':
                    rotate = (Actions.RotateBy(180, duration=0))
                elif self.direction == 'right':
                    rotate = (Actions.RotateBy(-90, duration=0))
                elif self.direction == 'down':
                    rotate = (Actions.RotateBy(0, duration=0))
                elif self.direction == 'left':
                    rotate = (Actions.RotateBy(90, duration=0))
                else:
                    rotate = (Actions.RotateBy(0, duration=0))
                fadeOut = Actions.FadeOut(1)
                self.do(fadeOut)
                fadeIn = Actions.FadeIn(1)
                playerDead = cocos.sprite.Sprite('GameImages/player-extraextrasmall-white-dead.png')
                playerDead.do(rotate + fadeIn)
                self.add(playerDead, z=10)
                self.is_alive = False

            # ALARM VISUAL OVERLAY
            if self.health <= 50 and self.alarmCount <= 10:
                fadeOut = Actions.FadeOut(0.1)
                viewAlarm = cocos.sprite.Sprite('GameImages/viewAlarmB25.png')
                self.add(viewAlarm, z=10)
                viewAlarm.do(Actions.Repeat(fadeOut))
                self.alarmCount += 1
                print(str(self.alarmCount) + ' alarms')
                # ALARM SOUND - SUCCESSIVE INSTANCES OF SOUND WILL STACK
                #alarmChannel.play(sound_alarm, -1)
                sound_alarm.play(-1)

        # COLLISION VS WALL SEGMENTS
        if isinstance(other, WallSegmentHorizontal) or isinstance(other, WallSegmentVertical) or isinstance(other,
                WallVertical) or isinstance(other, WallHorizontal):
            if self.direction == 'up':
                self.position = (self.position[0], self.position[1] - 10)
            elif self.direction == 'right':
                self.position = (self.position[0] - 10, self.position[1])
            elif self.direction == 'down':
                self.position = (self.position[0], self.position[1] + 10)
            elif self.direction == 'left':
                self.position = (self.position[0] + 10, self.position[1])
            self.cshape.center = self.position

    def kill_self(self):
        self.health = 0
        # CALL STOP TIMER
        # stop_timer or restart_timer in GameLayer

    def show_view(self):
        # VIEW OVERLAY
        view_sprite = cocos.sprite.Sprite('GameImages/player-small-with-view7.png')
        view_sprite.position = 0, 0
        self.add(view_sprite, z=1) #
        rotate = (Actions.RotateBy(360, duration=10))
        view_sprite.do(Actions.Repeat(rotate))
        scaleUp = Actions.ScaleBy(1.5)
        scaleDown = Actions.ScaleBy(1 / 1.5)
        view_sprite.do(Actions.Repeat(scaleUp + scaleDown))
        view_sprite = cocos.sprite.Sprite('GameImages/player-small-with-view7b.png')
        view_sprite.position = 0, 0
        self.add(view_sprite, z=1) #
        rotate = (Actions.RotateBy(-360, duration=18))
        view_sprite.do(Actions.Repeat(rotate))
        scaleUp = Actions.ScaleBy(1.5)
        scaleDown = Actions.ScaleBy(1 / 1.5)
        view_sprite.do(Actions.Repeat(scaleUp + scaleDown))
        fadeIn = Actions.FadeIn(10)
        fadeOut = Actions.FadeOut(10)
        view_sprite.do(Actions.Repeat(fadeIn + fadeOut))

    def show_flash(self):
        fadeOut = Actions.FadeOut(1)
        flashOverlay = cocos.sprite.Sprite('GameImages/viewFlash.png')
        self.add(flashOverlay, z=100)
        flashOverlay.do(fadeOut)

# VIEW
class View(cocos.sprite.Sprite):
    def __init__(self, image, x, y, ):
        super(View, self).__init__(image)
        sprite_size = 30
        self.position = Euclid.Vector2(x, y)
        self.cshape = CollisionManager.AARectShape(self.position,
                                     sprite_size * 0.5,
                                     sprite_size * 0.5)

    def move(self, offset):
        self.position += offset
        self.cshape.center += offset


# ENDPOINT
class EndPoint(Actor):
    def __init__(self, x, y):
        super(EndPoint, self).__init__('GameImages/endpoint-star.png', x, y)
        self.scale = 0.15
        self.opacity = 100
        self.soundPlaying = False

    def collide(self, other):
        print("You Win")
        if self.soundPlaying == False:
            reserveChannel.play(sound_cheer)
            self.soundPlaying = True
        hud_layer.show_you_win()


# PICKUP
class Pickup(Actor):
    def __init__(self, x, y, pickup_type):
        super(Pickup, self).__init__('GameImages/pickup-blue.png', x, y)
        self.type = pickup_type
        scaleUp = Actions.ScaleBy(1.25)
        scaleDown = Actions.ScaleBy(1 / 1.25)
        self.do(Actions.Repeat(Actions.Speed((scaleUp + scaleDown), 25)))
        if pickup_type == 'KEY':
            self.show_key()

    def collide(self, other):
        collisonChannel.play(sound_magic)
        print (self.type)
        hud_layer.show_screen_message(self.type.upper())

    def show_key(self):
        view_sprite = cocos.sprite.Sprite('GameImages/pickup-white.png')
        self.add(view_sprite, z=2)
        scaleUp = Actions.ScaleBy(1.35)
        scaleDown = Actions.ScaleBy(1 / 1.35)
        self.do(Actions.Repeat(Actions.Speed((scaleUp + scaleDown), 25)))

# PORTAL
class Portal(Actor):
    def __init__(self, x, y):
        super(Portal, self).__init__('GameImages/pickup-purple.png', x, y)
        fadeIn = Actions.FadeIn(1)
        fadeOut = Actions.FadeOut(1)
        scaleUp = Actions.ScaleBy(1.25)
        scaleDown = Actions.ScaleBy(1 / 1.25)
        sequencePickupPortal = Actions.Speed((scaleUp + scaleDown), 50)
        self.do(Actions.Repeat(sequencePickupPortal | (fadeIn + fadeOut)))

    def collide(self, other):
        collisonChannel.play(sound_teleport)
        hud_layer.show_screen_message('PORTAL?')

        #fadeOut = Actions.FadeOut(1)
        #flashOverlay = cocos.sprite.Sprite('GameImages/viewFlash2.png')
        #self.add(flashOverlay, z=100000)

        # FLASH WHEN PORTAL ACTIVATES
        # call show_flash from Player
        #############################################
        #Player.show_flash()


# ENEMY; INHERITS FROM ACTOR
class Enemy(Actor):
    def __init__(self, x, y):
        super(Enemy, self).__init__('GameImages/enemy-black.png', x, y)
        self.health = 100
        self.x = x
        self.y = y
        rotate = (Actions.RotateBy(360, duration=0.1))
        self.do(Actions.Repeat(rotate))

    def collide(self, other):
        # ENEMY HEALTH BAR
        # does not display correctly yet - can be fixed to place healthbar atop enemy
        if self.health > 0:
            print("Enemy health is " + str(self.health))
            self.health -= 1
            collisonChannel.play(sound_hit)
        fadeOut = Actions.FadeOut(1)
        if self.health > 0:
            healthBarGreen = cocos.sprite.Sprite('GameImages/health-green.png')
            self.add(healthBarGreen, z=100)
            healthBarGreen.position = self.x, self.y
            healthBarRed = cocos.sprite.Sprite('GameImages/health-red.png')
            self.add(healthBarRed, z=101)
            healthBarRed.position = self.x, self.y
            if self.health <= 3:
                healthBarRed.scale_x = 1.0
                healthBarGreen.scale_x = 0.0
            elif self.health <= 10:
                healthBarRed.scale_x = 0.9
            elif self.health <= 20:
                healthBarRed.scale_x = 0.8
            elif self.health <= 30:
                healthBarRed.scale_x = 0.7
            elif self.health <= 40:
                healthBarRed.scale_x = 0.6
            elif self.health <= 50:
                healthBarRed.scale_x = 0.5
            elif self.health <= 60:
                healthBarRed.scale_x = 0.4
            elif self.health <= 70:
                healthBarRed.scale_x = 0.3
            elif self.health <= 80:
                healthBarRed.scale_x = 0.2
            elif self.health <= 90:
                healthBarRed.scale_x = 0.1
            elif self.health <= 100:
                healthBarRed.scale_x = 0.0
            healthBarGreen.do(fadeOut)
            healthBarRed.do(fadeOut)

# MESSAGE TRIGGER POINT
class TriggerPoint(Actor):
    def __init__(self, x, y, text):
        super(TriggerPoint, self).__init__('GameImages/helpMessageTrigger.png', x, y)
        self.opacity = 0
        self.text = text

    def collide(self, other):
        hud_layer.show_screen_message(self.text)


# BORDER VERTICAL
class WallVertical(Actor):
    def __init__(self, x, y):
        super(WallVertical, self).__init__('GameImages/wallVerticalWhite.png', x, y)
        self.opacity = 100
        sprite_size = 28, 692
        self.cshape = CollisionManager.AARectShape(self.position,
                                     sprite_size[0] * 0.5,
                                     sprite_size[1] * 0.5)

    def collide(self, other):
        Player.collide_Wall = True


# BORDER HORIZONTAL
class WallHorizontal(Actor):
    def __init__(self, x, y):
        super(WallHorizontal, self).__init__('GameImages/wallHorizontalWhite.png', x, y)
        self.opacity = 100
        sprite_size = 1030, 28
        self.cshape = CollisionManager.AARectShape(self.position,
                                     sprite_size[0] * 0.5,
                                     sprite_size[1] * 0.5)

    def collide(self, other):
        Player.collide_Wall = True


# WALL SEGMENT VERTICAL
class WallSegmentVertical(Actor):
    def __init__(self, x, y):
        super(WallSegmentVertical, self).__init__('GameImages/wallSegmentVerticalWhite.png', x, y)
        self.opacity = 100
        sprite_size = 28, 95
        self.cshape = CollisionManager.AARectShape(self.position,
                                     sprite_size[0] * 0.5,
                                     sprite_size[1] * 0.5)

    def collide(self, other):
        #print(self.x)
        Player.collide_Wall = True


# WALL SEGMENT HORIZONTAL
class WallSegmentHorizontal(Actor):
    def __init__(self, x, y):
        super(WallSegmentHorizontal, self).__init__('GameImages/wallSegmentHorizontalWhite.png', x, y)
        self.opacity = 100
        sprite_size = 95, 28
        self.cshape = CollisionManager.AARectShape(self.position,
                                     sprite_size[0] * 0.5,
                                     sprite_size[1] * 0.5)

    def collide(self, other):
        #print(self.x)
        Player.collide_Wall = True


# GAMELAYER
class GameLayer(cocos.layer.Layer):
    is_event_handler = True

    def __init__(self):
        super(GameLayer, self).__init__()
        width, height = cocos.director.director.get_window_size()
        self.width = width
        self.height = height
        coll_manager_grid_size_in_pixels = 32
        self.collman = CollisionManager.CollisionManagerGrid(0, width, 0, height, coll_manager_grid_size_in_pixels,
            coll_manager_grid_size_in_pixels)
        self.create_endpoint()
        self.create_pickups()
        self.create_portals()
        self.create_enemies()
        self.create_triggerpoint()
        self.create_wall_boundaries()
        self.create_wall_segments_horizontal()
        self.create_wall_segments_vertical()
        self.create_view()
        self.schedule(self.update)
        moveR = Actions.MoveBy((1, 5), duration=1)
        moveL = Actions.MoveBy((-1, -5), duration=1)
        self.do(Actions.Repeat(Actions.Speed((moveR + moveL), 25)))

        self.hud = HUD
        self.timer = self._timer = 10000
        self.lives = 1
        self.key_found = False

    def on_key_press(self, k, _):
        Player.KEYS_PRESSED[k] = 1

    def on_key_release(self, k, _):
        Player.KEYS_PRESSED[k] = 0

    def update(self, dt):
        self.collman.clear()
        for _, node in self.children:
            self.collman.add(node)
        for other in self.collman.iter_colliding(self.player):
            # self.remove(other)
            other.collide(other)
            self.player.collide(other)
            if isinstance(other, Enemy) and other.health <= 0:
                self.remove(other)
                print("Enemy is DEAD")
            elif isinstance(other, Pickup):
                self.remove(other)
                #print(dir(Pickup))
                # CHECK IF PICKUP IS KEY
                ###################################################
                #if pickup_type == 'KEY':
                #    print("Key received!")
                #    self.key_found = True
                #else:
                #    print("Pickup received!")
            elif isinstance(other, Portal):
                # PORTAL TELEPORT
                # call teleport from Player
                ###################################################
                ###################################################
                self.remove(other)
                print("Portal triggered!")
        for _, node in self.children:
            node.update(dt)
        #health = Player.checkHealth()
        if self.timer > 0:  #  and health > 0
            self.timer -= 1
        hud_layer.update_timer(self.timer)
        if self.timer <= 0:
            pass
            # TRIGGER GAME OVER
            # call kill_self from Player to reduce lives & stop/restart timer; pass lives to check
            ###################################################
            ###################################################

    def stop_timer(self):
        self.timer =0
        print('timer stopped')
        print(self.timer)

    def create_view(self):
        width = 460
        height = 400
        self.player = Player(width, height, 'PlayerViewInstance')
        self.add(self.player)

    def create_endpoint(self):
        # RANDOMISES ENDPOINT
        random_endpoint_selector = randomiser
        print(random_endpoint_selector) # CONFIRMS ENDPOINT SELECTED BY RANDOMISER
        self.endPoint = EndPoint(endpoint_locations[random_endpoint_selector][0],
                                 endpoint_locations[random_endpoint_selector][1])  # keep for collision shape
        self.add(self.endPoint)

    def create_pickups(self):
        if randomiser % 2 != 0:
            pickup1 = 'HEALTH+'
            pickup2 = 'TIME+'
            pickup3 = 'POWER+'
            pickup4 = 'KEY'
        else:
            pickup1 = 'POWER+'
            pickup2 = 'HEALTH+'
            pickup3 = 'TIME+'
            pickup4 = 'KEY'
        if randomiser % 2 != 0:
            pickups = ((100, 400, pickup1), (370, 120, pickup2), (650, 590, pickup3), (935, 120, pickup4))
        else:
            pickups = ((500, 120, pickup4), (80, 680, pickup3), (650, 685, pickup2), (940, 420, pickup1))
        # SHOW ALL
        #pickups = ((100, 400, pickup1), (370, 120, pickup2), (650, 590, pickup1), (935, 120, pickup2), (80, 120, pickup1), (80, 680, pickup2), (650, 685, pickup1), (940, 420, pickup2))
        i = 0
        for _ in pickups:
            self.addPickups = Pickup((pickups[i])[0], (pickups[i])[1], (pickups[i])[2])
            self.add(self.addPickups)
            i += 1

    def create_portals(self):
        if randomiser % 2 != 0:
            portals = ((370, 300), (520, 115), (560, 300), (840, 490))
        else:
            portals = ((80, 490), (550, 585), (750, 115), (840, 300))
        # SHOW ALL
        #portals = ((370, 300), (550, 585), (560, 300), (840, 490),(80, 490), (520, 115), (750, 115), (745, 590))
        i = 0
        for _ in portals:
            self.addPortals = Portal((portals[i])[0], (portals[i])[1])
            self.add(self.addPortals)
            i += 1

    def create_triggerpoint(self):
        triggerpoint = ((150, 205, 'WTF'), (200, 400, 'HELP'), (300, 685, 'OMG'), (700, 400, 'PLZ'))
        i = 0
        for _ in triggerpoint:
            self.addTriggerPoints = TriggerPoint((triggerpoint[i])[0], (triggerpoint[i])[1], (triggerpoint[i])[2])
            self.add(self.addTriggerPoints)
            i += 1

    def create_wall_boundaries(self):
        # EXTERNAL BORDERS HORIZONTAL
        borderHorizontal = ((508, 735), (508, 68))
        i = 0
        for _ in borderHorizontal:
            self.addBorderHorizontal = WallHorizontal((borderHorizontal[i])[0], (borderHorizontal[i][1]))
            self.add(self.addBorderHorizontal)
            i += 1

        # EXTERNAL BORDERS VERTICAL
        borderVertical = ((32, 400), (985, 400))
        i = 0
        for _ in borderVertical:
            self.addBorderVertical = WallVertical((borderVertical[i])[0], (borderVertical[i][1]))
            self.add(self.addBorderVertical)
            i += 1

    def create_wall_segments_vertical(self):
        # SET VERTICAL MAZE WALL SEGMENT LOCATIONS
        # SEGMENT COORDINATES // size = 28 x 95 px
        wallSegmentLocation = (
            (415, 675), (605, 675), (890, 675),
            (225, 595), (700, 595), (795, 595),
            (130, 500), (320, 500), (415, 500), (605, 500), (700, 490), (795, 490),
            (320, 405), (510, 405), (605, 395), (890, 395),
            (130, 310), (225, 310), (510, 310),
            (225, 205), (510, 205), (795, 215),
            (130, 120), (320, 120), (415, 120), (605, 120), (795, 120), (890, 120)
        )
        i = 0
        for _ in wallSegmentLocation:
            self.addWallSegmentVertical = WallSegmentVertical((wallSegmentLocation[i])[0], (wallSegmentLocation[i][1]))
            self.add(self.addWallSegmentVertical)
            i += 1

    def create_wall_segments_horizontal(self):
        # SET ORIZONTAL MAZE WALL SEGMENT LOCATIONS
        # SEGMENT COORDINATES // size = 95 x 28 px
        wallSegmentLocation = (
            (93, 635), (355, 635), (545, 635), (668, 635),
            (190, 540), (475, 540), (760, 540), (922, 540),
            (93, 445), (260, 445), (448, 445), (573, 445), (858, 445),
            (190, 350), (355, 350), (450, 350), (668, 350), (830, 350),
            (93, 255), (285, 255), (450, 255), (640, 255), (735, 255), (925, 255),
            (545, 160), (668, 160)
        )
        i = 0
        for _ in wallSegmentLocation:
            self.addWallSegmentHorizontal = WallSegmentHorizontal((wallSegmentLocation[i])[0],
                                                                  (wallSegmentLocation[i][1]))
            self.add(self.addWallSegmentHorizontal)
            i += 1

    def create_enemies(self):
        # SET ENEMY LOCATIONS
        if randomiser % 2 != 0:
            enemyLocation = (
                (200, 110), (250, 490), (750, 310), (900, 590), (440, 590)
            )
        else:
            enemyLocation = (
                (400, 590), (550, 490), (750, 310), (320, 210), (140, 590)
            )
        #SHOW ALL
        #enemyLocation = ((400, 590), (550, 490), (750, 310), (320, 210), (140, 590), (200, 110), (250, 490), (750, 310), (900, 590), (440, 590))
        i = 0
        for _ in enemyLocation:
            self.addEnemy = Enemy(enemyLocation[i][0], enemyLocation[i][1])
            self.add(self.addEnemy)
            i += 1

    def restart_timer(self):
        ticks = 10000
        self.timer = ticks


class OverlayLayer(cocos.layer.Layer):
    '''OBSCURES LAYERS BENEATH'''
    def __init__(self):
        super(OverlayLayer, self).__init__()
        width, height = cocos.director.director.get_window_size()
        self.width = width
        self.height = height

    # DISPLAY ENDPOINT
    def show_endpoint(self):
        random_endpoint_selector = randomiser
        print(random_endpoint_selector) # CONFIRMS ENDPOINT SELECTED BY RANDOMISER
        endpoint_sprite = cocos.sprite.Sprite('GameImages/endpoint-star.png')
        endpoint_sprite.position = endpoint_locations[random_endpoint_selector][0], \
                                   endpoint_locations[random_endpoint_selector][1]
        endpoint_sprite.scale = 0.20
        self.add(endpoint_sprite, z=1)
        rotate = (Actions.RotateBy(360, duration=1))
        fadeIn = Actions.FadeIn(1)
        fadeOut = Actions.FadeOut(1)
        scaleDown = Actions.ScaleBy(0.25)
        actionSequence = Actions.Repeat(rotate) | (Actions.Repeat(fadeIn + fadeOut)) | Actions.Repeat(Actions.Speed((scaleDown), 1.1))
        endpoint_sprite.do(actionSequence)

    # DISPLAY PLAYER HEALTH BAR
    def healthBar(self, health):
        print('My health is ' + str(health))
        fadeOut = Actions.FadeOut(1)
        if health > 0:
            healthBarGreen = cocos.sprite.Sprite('GameImages/my-health-green.png')
            self.add(healthBarGreen, z=100)
            healthBarGreen.position = self.width / 2, 775
            healthBarRed = cocos.sprite.Sprite('GameImages/my-health-red.png')
            self.add(healthBarRed, z=101)
            healthBarRed.position = self.width / 2, 775
            if health <= 3:
                healthBarRed.scale_x = 1.0
                healthBarGreen.scale_x = 0.0
            elif health <= 10:
                healthBarRed.scale_x = 0.9
            elif health <= 20:
                healthBarRed.scale_x = 0.8
            elif health <= 30:
                healthBarRed.scale_x = 0.7
            elif health <= 40:
                healthBarRed.scale_x = 0.6
            elif health <= 50:
                healthBarRed.scale_x = 0.5
            elif health <= 60:
                healthBarRed.scale_x = 0.4
            elif health <= 70:
                healthBarRed.scale_x = 0.3
            elif health <= 80:
                healthBarRed.scale_x = 0.2
            elif health <= 90:
                healthBarRed.scale_x = 0.1
            elif health <= 100:
                healthBarRed.scale_x = 0.0
                # LIFE REDUCTION
                # call kill_self from Player to reduce lives & stop/restart timer
            healthBarGreen.do(fadeOut)
            healthBarRed.do(fadeOut)


# DISPLAY TITLES, LIVES/HEALTH, POINTS
class HUD(cocos.layer.Layer):
    def __init__(self):
        super(HUD, self).__init__()
        self.w, self.h = cocos.director.director.get_window_size()
        self.generate_labels()
        self.generate_menu()


    def generate_menu(self):
            # BASE POSITION
            position_x = self.w / 2 + 220
            position_y = self.h / 2 - 100

            # MENU COLLECTIVE ACTIONS
            fadeIn = Actions.FadeIn(1)
            fadeOut = Actions.FadeOut(1)
            fadeInOut = Actions.FadeIn(6) + Actions.FadeOut(9)

            # MENU INDIVIDUAL ACTIONS
            scaleUpKey = Actions.ScaleBy(1.5)
            scaleDownKey = Actions.ScaleBy(1 / 1.5)
            scaleUpPickup = Actions.ScaleBy(1.25)
            scaleDownPickup = Actions.ScaleBy(1 / 1.25)
            sequencePickup = Actions.Repeat(Actions.Speed((scaleUpPickup + scaleDownPickup), 25))
            sequencePickupPortal = Actions.Repeat(Actions.Speed((scaleUpPickup + scaleDownPickup), 50))
            sequenceKey = Actions.Repeat(Actions.Speed((scaleUpKey + scaleDownKey), 25))
            sequencePortal = Actions.Repeat(Actions.Speed((fadeIn + fadeOut), 1))
            sequenceEndpoint = Actions.Repeat(Actions.RotateBy(360, duration=1))
            sequenceEnemy = Actions.Repeat(Actions.RotateBy(360, duration=0.1))

            # GENERATE MENU PANELS
            menuPanel = cocos.sprite.Sprite('GameImages/menuPanelShard.png')
            menuPanelLeft = cocos.sprite.Sprite('GameImages/menuPanelShard.png')
            menuPanelRight = cocos.sprite.Sprite('GameImages/menuPanelShard.png')
            menuPanel.scale = 0.4
            menuPanelLeft.scale = 1
            menuPanelRight.scale = 1
            menuPanel.position = position_x, position_y + 15
            menuPanelLeft.position = position_x - 220, position_y - 380
            menuPanelRight.position = position_x + 430, position_y - 170
            self.add(menuPanel, z=100)
            self.add(menuPanelLeft, z=101)
            self.add(menuPanelRight, z=101)
            menuPanel.do(fadeInOut)
            menuPanelLeft.do(fadeInOut)
            menuPanelRight.do(fadeInOut)

            # GENERATE KEY IMAGES
            keyImageObjects = ['0', '1', '2', '3', '4']
            key_images = (
                ('GameImages/pickup-white.png', position_x, position_y + 80),
                ('GameImages/pickup-purple.png', position_x - 280, position_y - 120),
                ('GameImages/pickup-blue.png', position_x - 240, position_y - 170),
                ('GameImages/endpoint-star.png', position_x - 200, position_y - 220),
                ('GameImages/enemy-black.png', position_x - 160, position_y - 260)
            )
            i = 0
            for _ in key_images:
                keyImageObjects[i] = cocos.sprite.Sprite(key_images[i][0])
                keyImageObjects[i].position = key_images[i][1], key_images[i][2]
                if keyImageObjects[i] == keyImageObjects[0]:
                    keyImageObjects[i].do(sequenceKey)
                elif keyImageObjects[i] == keyImageObjects[1]:
                    pass
                    keyImageObjects[i].do(sequencePickupPortal)
                    #keyImageObjects[i].do(sequencePortal)
                    # GHOST EFFECT - DOUBLE FADE BREAKS SEQUENCE?
                elif keyImageObjects[i] == keyImageObjects[2]:
                    keyImageObjects[i].do(sequencePickup)
                elif keyImageObjects[i] == keyImageObjects[3]:
                    keyImageObjects[i].scale = 0.15
                    keyImageObjects[i].do(sequenceEndpoint)
                elif keyImageObjects[i] == keyImageObjects[4]:
                    keyImageObjects[i].do(sequenceEnemy)
                else:
                    pass
                keyImageObjects[i].do(fadeInOut)
                self.add(keyImageObjects[i], z=101)
                i += 1

            # GENERATE KEY TEXT
            keyTextObjects = ['0', '1', '2', '3', '4', '5', '6']
            key_text = (
                ('KEY', position_x, position_y + 125),
                ('PORTAL', position_x - 235, position_y - 75),
                ('PICKUP', position_x - 195, position_y - 125),
                ('ENDPOINT', position_x - 150, position_y - 170),
                ('ENEMY', position_x - 115, position_y - 220),
                ('CISC-220', position_x + 155, position_y - 155),
                ('NON-GENERIC MAZE', position_x + 215, position_y - 155)
            )
            i = 0
            for _ in key_text:
                keyTextObjects[i] = cocos.text.Label(key_text[i][0],
                                                       font_name='Helvetica',
                                                       font_size=11,
                                                       anchor_x='center',
                                                       anchor_y='center',
                                                       color=(255, 255, 255, 85))
                keyTextObjects[i].position = key_text[i][1], key_text[i][2]
                if keyTextObjects[i] == keyTextObjects[0]:
                    pass
                elif keyTextObjects[i] == keyTextObjects[5] or keyTextObjects[i] == keyTextObjects[6]:
                    keyTextObjects[i].do(Actions.Rotate(45, duration=0))
                else:
                    keyTextObjects[i].do(Actions.Rotate(-45, duration=0))
                keyTextObjects[i].do(fadeInOut)
                self.add(keyTextObjects[i], z=101)
                i += 1

            # GENERATE INFORMATIVE TEXT
            text_label = (
                ('Welcome to the recesses of your mind', position_x, position_y + 30),
                ('The key to your sanity is trapped', position_x, position_y + 10),
                ('within a crystal of your colour', position_x, position_y - 10),
                ('Find it to unlock your escape', position_x, position_y - 30),
                ('', position_x, position_y - 50)
            )
            textLabelObjects = ['0', '1', '2', '3', '4']
            i = 0
            for _ in text_label:
                textLabelObjects[i] = cocos.text.Label(text_label[i][0],
                                              font_name='Helvetica',
                                              font_size=11,
                                              anchor_x='center',
                                              anchor_y='center',
                                              color=(255, 255, 255, 85))
                textLabelObjects[i].position = text_label[i][1], text_label[i][2]
                self.add(textLabelObjects[i], z=101)
                textLabelObjects[i].do(fadeInOut)
                i += 1

            # THREADING TIMER FOR REMOVING MENU OBJECTS FROM MEMORY <<

            # print('MENU SUCCESSFULLY GENERATED')
            #while textLabelObjects[3].are_actions_running() == True:
                # KEEP CHECKING FOR OPPORTUNITY TO CLEAR MEMORY
                # CLEAR LABEL OBJECTS
            #    Time.sleep(5)
            #    if textLabelObjects[3].are_actions_running() == False:
            #        i = 0
            #        for _ in textLabelObjects:
            #            self.remove(textLabelObjects[i])
            #            i += 1
                    # CLEAR KEY TEXT OBJECTS
            #        i = 0
            #        for _ in keyTextObjects:
            #            self.remove(keyTextObjects[i])
            #            i += 1
                    # CLEAR KEY IMAGE OBJECTS
            #        i = 0
            #        for _ in keyImageObjects:
            #            self.remove(keyImageObjects[i])
            #            i += 1
            #        print('MENU CLEARED FROM MEMORY')


    # GENERATE HUD LABELS TOP + BOTTOM
    def generate_labels(self):
        # GENERATE HUD LABELS TOP
        self.HUDlabelObjects = ['0', '1', '2', '3', '4']
        HUD_labels = (
            ('LIVES:01', 104, self.h * 0.75 + 178),
            ('KEY:LOST', 330, self.h * 0.75 + 178),
            ('00:', self.w * 0.5 + 422, self.h * 0.75 + 178),
            ('NON-GENERIC MAZE', 630, self.h * 0.75 + 178)
        )
        i = 0
        for _ in HUD_labels:
            self.HUDlabelObjects[i] = cocos.text.Label(HUD_labels[i][0],
                                              font_name='Helvetica',
                                              font_size=20,
                                              anchor_x='center',
                                              anchor_y='center',
                                              color=(255, 255, 255, 85))
            self.HUDlabelObjects[i].position = HUD_labels[i][1], HUD_labels[i][2]
            self.add(self.HUDlabelObjects[i], z=2)
            i += 1
            self.livesLabel = self.HUDlabelObjects[0]
            self.keyLabel = self.HUDlabelObjects[1]
            self.timeLabel = self.HUDlabelObjects[2]

        # GENERATE HUD LABELS BOTTOM
        HUD_labels_bottom = (
            ('[ENTER] FOR MENU', 113, self.h * 0.75 - 570),
            ('[ESC] TO QUIT', 938, self.h * 0.75 - 570),
            #('[ENTER] FOR MENU    |    [ESC] TO QUIT', 179, self.h * 0.75 - 570),
            #('', 938, self.h * 0.75 - 570)
        )
        i = 0
        for _ in HUD_labels_bottom:
            self.titleText = cocos.text.Label(HUD_labels_bottom[i][0],
                                              font_name='Helvetica',
                                              font_size=10,
                                              anchor_x='center',
                                              anchor_y='center',
                                              color=(255, 255, 255, 85))
            self.titleText.position = HUD_labels_bottom[i][1], HUD_labels_bottom[i][2]
            self.add(self.titleText, z=500)
            i += 1

    # UPDATE TIMER LABEL
    def update_timer(self, time):
        timey = str("%04d" % time)
        self.timeLabel.element.text = '00:' + timey[0:2] + ':' + timey[2:4]
        if time < 2500:
            if self.timeLabel.are_actions_running() != True:
                fadeOut = Actions.FadeOut(1)
                self.timeLabel.do(Actions.Repeat(fadeOut))

    # SHOW SCREEN MESSAGE
    def show_screen_message(self, passedText):
        w, h = cocos.director.director.get_window_size()
        if passedText == 'KEY':
            label = cocos.text.Label(passedText + '!',
                                     font_name='Helvetica',
                                     font_size=250,
                                     anchor_x='center', anchor_y='center',
                                     color=(255, 255, 255, 100))
            # UPDATE KEY LABEL
            self.keyLabel.element.text = 'KEY:FOUND'
        elif passedText == 'POWER+' or passedText == 'HEALTH+' or passedText == 'TIME+' or passedText == 'PORTAL?':
            label = cocos.text.Label(passedText,
                                     font_name='Helvetica',
                                     font_size=100,
                                     anchor_x='center', anchor_y='center',
                                     color=(255, 255, 255, 100))
        else:
            label = cocos.text.Label(passedText,
                                     font_name='Helvetica',
                                     font_size=200,
                                     anchor_x='center', anchor_y='center',
                                     color=(255, 0, 0, 100))
        self.add(label, z=100)
        label.position = w * 0.5, h * 0.5
        if passedText == 'DEAD':
            label.position = w * 0.5, h * 0.5 + 100
            fadeOut = None
        elif len(passedText) >= 20:
            label.position = w * 0.5, h * 0.5 + 100
            fadeOut = Actions.FadeOut(6)
        elif len(passedText) >= 10:
            fadeOut = Actions.FadeOut(3)
        else:
            fadeOut = Actions.FadeOut(1)
        if passedText != 'DEAD':
            label.do((fadeOut))

    # GAME OVER
    def show_game_over(self):
        w, h = cocos.director.director.get_window_size()
        red = (50, 75, 100, 255)
        label = cocos.text.Label('You Lost!',
                                 font_name='Helvetica',
                                 font_size=30,
                                 anchor_x='center', anchor_y='center',
                                 color=red)
        label.position = w * 0.5, h * 0.5
        self.add(label, z=100)

    # YOU WIN
    def show_you_win(self):
        w, h = cocos.director.director.get_window_size()

        label = cocos.text.Label('You Won!',
                                 font_name='Helvetica',
                                 font_size=80,
                                 anchor_x='center', anchor_y='center',
                                 color=(255, 255, 255, 255))
        label.position = w * 0.5, h * 0.5
        self.add(label, z=100)
        rotate = (Actions.RotateBy(360, duration=1))
        fadeIn = Actions.FadeIn(1)
        fadeOut = Actions.FadeOut(1)
        scaleUp = Actions.ScaleBy(9)
        label.do(Actions.Repeat(rotate))
        label.do(Actions.Repeat(fadeIn + fadeOut))
        label.do(Actions.Repeat(scaleUp))


# BACKGROUND
class Background(cocos.layer.ColorLayer):
    def __init__(self):
        super(Background, self).__init__(0, 0, 0, 255)
        w, h = cocos.director.director.get_window_size()

        # BACKGROUND
        background_sprite = cocos.sprite.Sprite('GameImages/bg0.jpg')
        background_sprite.set_position(w / 2, h / 2)
        rotate = (Actions.RotateBy(360, duration=0.25))
        background_sprite.do(Actions.Repeat(rotate))
        background_sprite.scale = 1
        self.add(background_sprite, z=0)


# MAIN
if __name__ == '__main__':
    cocos.director.director.init(caption='NON-GENERIC MAZE',
                                 width=1024, height=800)
    backgroundChannel.play(song1, -1)
    main_scene = cocos.scene.Scene()
    layer = GameLayer()
    hud_layer = HUD()
    overlay_layer = OverlayLayer()
    overlay_layer.show_endpoint()
    hud_layer.show_screen_message('ESCAPE')
    background = Background()
    main_scene.add(layer, z=0)
    main_scene.add(background, z=-1)
    main_scene.add(hud_layer, z=500)
    main_scene.add(overlay_layer, z=3)
    cocos.director.director.run(main_scene)